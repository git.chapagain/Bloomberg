=========================================================
Project created under the 'berg' name using Maven
=========================================================

1. Please check and change the application.properties file in following:
	a. #Filepath
	b. #Db setup

2. Execute command: java -jar berg-0.0.1-SNAPSHOT.war (If you use .war file)

4. If you go through whole project, use maven command:
	-Go to project folder and open command terminal.
	-Enter : mvn clean package install -DskipTests=true (For clean and build the project).
	-Enter : mvn spring-boot:run -DskipTests=true (For Run the project)

3. After successfully tomcat start, Enter URL localhost:8080 in browser.
   Generate sample file by clicking 'Generate Sample' button, it store generated file in 'Sample' folder. 

5. Then Upload file from 'Sample' folder. It first save to 'Uploaded' folder, after successfully process and insert
	into database it will move to 'Completed' folder.


Note:
======
Inside application.properties:

#Filepath
app.filepath.upload-location= E:\\Bloomberg\\Uploaded\\
app.filepath.moved-location= E:\\Bloomberg\\Completed\\
app.filepath.sample-location=E:\\Bloomberg\\Sample\\	


Uploaded: Use for store the files when we upload from view page. After insert into database these file will 
	  be move to Complete folder.
Completed: Use for store the files which are completly insert into database.
Sample: Use for sample file generated from view panel. By click 'Generate Sample' button.

------------
Git link
-------------
https://github.com/git-chapagain/Bloomberg.git