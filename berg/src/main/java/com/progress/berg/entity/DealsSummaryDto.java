package com.progress.berg.entity;

import java.io.Serializable;
import java.util.Date;

public class DealsSummaryDto implements Serializable {
    private String fileName;
    private Long totalImportedDeal;
    private Long validDeal;
    private Long invalidDeal;
    private Date startTime;
    private Date endTime;
    private Long processTime;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getTotalImportedDeal() {
        return totalImportedDeal;
    }

    public void setTotalImportedDeal(Long totalImportedDeal) {
        this.totalImportedDeal = totalImportedDeal;
    }

    public Long getValidDeal() {
        return validDeal;
    }

    public void setValidDeal(Long validDeal) {
        this.validDeal = validDeal;
    }

    public Long getInvalidDeal() {
        return invalidDeal;
    }

    public void setInvalidDeal(Long invalidDeal) {
        this.invalidDeal = invalidDeal;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Long processTime) {
        this.processTime = processTime;
    }
}
