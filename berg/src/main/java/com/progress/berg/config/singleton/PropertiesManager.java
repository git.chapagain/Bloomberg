package com.progress.berg.config.singleton;

import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {

    private Properties props;

    public PropertiesManager() {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

        try (InputStream ios = classloader.getResourceAsStream("application.properties");) {
            props = new Properties();
            props.load(ios);

        } catch (Exception ex) {
            LoggerFactory.getLogger(PropertiesManager.class).error("Loading error application Properties", ex);
        }

    }

    public static PropertiesManager getInstance() {
        if (INSTANCE == null) {
            synchronized (PropertiesManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new PropertiesManager();
                }
            }
        }
        return INSTANCE;
    }

    private static PropertiesManager INSTANCE;

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public Properties getProperties() {
        return props;
    }

}
