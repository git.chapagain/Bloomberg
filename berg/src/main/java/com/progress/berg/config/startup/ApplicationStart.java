package com.progress.berg.config.startup;

import com.progress.berg.job.FileProcessThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStart {
    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */

    @Autowired
    FileProcessThread fileProcessThread;

    /**
     * Thread runwhile application start..it always check specifies folder and insert file into database
     * @param event
     */
    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        fileProcessThread.run();
    }

}
