/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.job;

import com.progress.berg.dao.ValidInvalidDealDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @param <T>
 * @author sds
 */

@Component
public class FileWriterThread<T> implements Runnable {

    Logger logger = LoggerFactory.getLogger(FileWriterThread.class);

    private boolean sucess = false;

    private final ValidInvalidDealDao validInvalidDealDao;

    private final List<T> batch;

    @Autowired
    public FileWriterThread(Optional<List<T>> batch, ValidInvalidDealDao validInvalidDealDao) {
        this.batch = batch.orElseGet(ArrayList::new);
        this.validInvalidDealDao = validInvalidDealDao;
    }

    @Override
    public void run() {
        while (!sucess) {
            try {
                validInvalidDealDao.saveDeals(batch);
                sucess = true;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                }

            }
        }

    }

}
