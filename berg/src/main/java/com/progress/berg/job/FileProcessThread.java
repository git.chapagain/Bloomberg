/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.job;

import com.progress.berg.config.singleton.PropertiesManager;
import com.progress.berg.dao.AccumulativeDealDao;
import com.progress.berg.dao.SourceDealDao;
import com.progress.berg.dao.ValidInvalidDealDao;
import com.progress.berg.dao.impl.ValidInvalidDealDaoImpl;
import com.progress.berg.entity.InvalidDeals;
import com.progress.berg.entity.SourceDeals;
import com.progress.berg.entity.ValidDeals;
import com.progress.berg.exception.FileNotFoundException;
import com.progress.berg.exception.InvalidDataException;
import com.progress.berg.utils.ValidationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sds
 *
 * We can make the process time in any fix time period (eg. load file at 5:00 PM every day) rather awalys run
 */
@Service
public class FileProcessThread implements Runnable {

    Logger logger = LoggerFactory.getLogger(FileProcessThread.class);

    private List<String> fileName;

    ExecutorService newFixedThreadPool = Executors.newCachedThreadPool();

    private AccumulativeDealDao accumulativeDealDao;

    private SourceDealDao sourceDealDao;

    private ValidInvalidDealDao validInvalidDealDao;

    @Autowired
    public FileProcessThread(AccumulativeDealDao accumulativeDealDao, SourceDealDao sourceDealDao, ValidInvalidDealDao validInvalidDealDao) {
        this.accumulativeDealDao = accumulativeDealDao;
        this.sourceDealDao = sourceDealDao;
        this.validInvalidDealDao = validInvalidDealDao;
    }

    @Override
    public void run() {
        while (true) {
            try {
                fileName = getAllAvailiableCSVFiles();
                if (fileName == null || fileName.isEmpty()) {
                    Thread.sleep(2000);
                }
                for (String file : fileName) {
                    logger.info("-----Intiating writing Jobs {} file-----", file);
//                    long start = System.currentTimeMillis();
                    SourceDeals saveFile = saveFile(file);
                    if (saveFile == null) {
                        return;
                    }

                    String originalPath = PropertiesManager.getInstance().getProperty("app.filepath.upload-location");
                    FileReader fileReader = getFile(file, originalPath);
                    try (BufferedReader br = new BufferedReader(fileReader);) {
                        saveAndValidateData(br, saveFile);
                        updateFileStatus(saveFile);
                    } catch (FileNotFoundException | IOException ex) {

                    }
                    logger.info("File upload and save sucessfully !");
//                    logger.info("------Total time to insert for {} file is {} second-------", file, (System.currentTimeMillis() - start) / 1000);
                    moveFile(file, originalPath);
                    Thread.sleep(2000);
                }

            } catch (Exception e) {
                logger.error("error", e);
            }
        }

    }

    public void saveAndValidateData(BufferedReader br, SourceDeals source) throws InterruptedException {
        List<ValidDeals> validDeals = new ArrayList<>();
        List<InvalidDeals> inValidDeals = new ArrayList<>();
        br.lines().forEach((data) -> {
            try {

                ValidDeals deals = ValidationUtil.getValidDeal(data);
                deals.setSourceDeals(source);
                validDeals.add(deals);
            } catch (InvalidDataException ex) {
                InvalidDeals invalidDeal = ValidationUtil.getInValidDeal(data);
                invalidDeal.setSourceDeals(source);
                inValidDeals.add(invalidDeal);
            }
        });

        submitBatch(validDeals);
        submitBatch(inValidDeals);
        increaseCurrencyCodeCount(validDeals, source);
    }


    private <T> void submitBatch(List<T> wholeData) {
        List<T> batch = new ArrayList<>();
        int count = 0;
        for (T t : wholeData) {
            batch.add(t);
            if (count % 25000 == 0) {
                FileWriterThread<T> batchWriterJob = new FileWriterThread<T>(Optional.ofNullable(batch), validInvalidDealDao);
                newFixedThreadPool.submit(batchWriterJob);
                batch = new ArrayList<>();
            }
            count++;
        }
        FileWriterThread<T> batchWriterJob = new FileWriterThread<T>(Optional.ofNullable(batch), validInvalidDealDao);
        newFixedThreadPool.submit(batchWriterJob);

    }

    public SourceDeals saveFile(String file) {
        if (sourceDealDao.isFileExist(file)) {
            return null;
        }
        SourceDeals source = new SourceDeals();
        source.setFileName(file);
        source.setStartDate(new Date());

        if (sourceDealDao.saveFile(source)) {
            return source;
        }
        return null;
    }

    private void increaseCurrencyCodeCount(List<ValidDeals> validDeals, SourceDeals sourceDeals) {
        accumulativeDealDao.increaseCount(getCountPerCurrency(validDeals), sourceDeals);
    }

    public Map<String, Long> getCountPerCurrency(List<ValidDeals> validDeals) {
        return validDeals.stream().collect(Collectors.groupingBy(ValidDeals::getFromCurrency, Collectors.counting()));
    }

    public void updateFileStatus(SourceDeals source) {
        source.setEndDate(new Date());
        sourceDealDao.updateFile(source);
    }


    public List<String> getAllAvailiableCSVFiles() {
        String uploadLocation = PropertiesManager.getInstance().getProperty("app.filepath.upload-location");
        File file = new File(uploadLocation);
        if (!file.exists()) {
            throw new FileNotFoundException("Location Not Found. Please check Location");
        }
        return Stream.of(file.listFiles((dir, name) -> name.toLowerCase().endsWith(".csv"))).map(File::getName).collect(Collectors.toList());

    }

    public void moveFile(String fileName, String location) {
        try {
            String moveLocation = PropertiesManager.getInstance().getProperty("app.filepath.moved-location");
            Path orginal = FileSystems.getDefault().getPath(location + fileName);
            Path move = FileSystems.getDefault().getPath(moveLocation + fileName);
            Files.move(orginal, move, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            logger.error("error", e);
        }
    }

    public FileReader getFile(String fileName, String location) {
        File file = new File(location + fileName);
        try {
            return new FileReader(file);
        } catch (java.io.FileNotFoundException ex) {
            throw new FileNotFoundException("file has been moved");
        }
    }

}
