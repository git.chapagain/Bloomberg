package com.progress.berg.controller;

import com.progress.berg.config.singleton.PropertiesManager;
import com.progress.berg.dao.SourceDealDao;
import com.progress.berg.entity.DealsSummaryDto;
import com.progress.berg.entity.ValidDeals;
import com.progress.berg.job.FileProcessThread;
import com.progress.berg.utils.SampleFileGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
@RequestMapping("/")
public class MainRestController {
    Logger logger = LoggerFactory.getLogger(MainRestController.class);

    @Autowired
    SourceDealDao dealDAO;

    @Autowired
    FileProcessThread fileProcessThread;


    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "uploadFile", method = RequestMethod.POST)
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("error", "Please select a file to upload");
            return "redirect:/";
        }
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(PropertiesManager.getInstance().getProperty("app.filepath.upload-location") + file.getOriginalFilename());
            Files.write(path, bytes);

//            fileProcessThread.doUpload(); //uncomment if thred is off
            redirectAttributes.addFlashAttribute("success",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.GET, value = "search")
    public String search(@RequestParam(value = "query") String query,
                         @RequestParam(value = "page", defaultValue = "1") Integer page,
                         ModelMap modelMap
    ) {
        if (page <= 0) {
            page = 1;
        }
        List<ValidDeals> allDealsByFileName = dealDAO.getAllDealsByFileName(query, page);
        System.out.println(allDealsByFileName.size());
        modelMap.put("query", query);
        modelMap.put("page", page);
        modelMap.put("deals", allDealsByFileName);
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "searchsummary")
    public String searchSummary(@RequestParam(value = "query") String query, ModelMap modelMap) {

        DealsSummaryDto summary = dealDAO.getDealSummaryDto(query);
        modelMap.put("query", query);
        modelMap.put("summary", summary);
        if (summary == null) {
            modelMap.put("error", "No records found !");
            logger.info("No records found !");
        }
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "sample")
    public String generateSample(ModelMap modelMap) {

        String result = SampleFileGenerator.generateSmaple();
        modelMap.put("success", result);
        return "index";
    }

}
