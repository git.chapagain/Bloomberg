package com.progress.berg.dao.impl;

import com.progress.berg.dao.ValidInvalidDealDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Repository
public class ValidInvalidDealDaoImpl<T> implements ValidInvalidDealDao {
    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    public ValidInvalidDealDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.emf = entityManagerFactory;
    }

    @Override
    public <T> void saveDeals(List<T> batch) {
        int count = 1;
        EntityManager em = emf.createEntityManager();
        EntityTransaction txn = em.getTransaction();
        txn.begin();
        for (T t : batch) {
            em.merge(t);
            if (count % 1000 == 0) {
                txn.commit();
                em.clear();
                txn.begin();
            }
            count++;
        }
        txn.commit();
    }
}
