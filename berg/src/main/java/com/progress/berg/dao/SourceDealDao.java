/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.dao;

import com.progress.berg.entity.DealsSummaryDto;
import com.progress.berg.entity.SourceDeals;
import com.progress.berg.entity.ValidDeals;

import java.util.List;


/**
 * @author sds
 */


public interface SourceDealDao {

    public boolean saveFile(SourceDeals source);

    public boolean updateFile(SourceDeals source);

    public boolean isFileExist(String fileName);

    public List<ValidDeals> getAllDealsByFileName(String fileName, Integer page);

    public DealsSummaryDto getDealSummaryDto(String fileName);

}
