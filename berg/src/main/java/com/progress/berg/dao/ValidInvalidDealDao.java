package com.progress.berg.dao;

import java.util.List;

public interface ValidInvalidDealDao {
    public <T> void saveDeals(List<T> batch);
}
