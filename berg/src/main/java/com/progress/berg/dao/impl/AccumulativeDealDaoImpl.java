/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.dao.impl;

import com.progress.berg.dao.AccumulativeDealDao;
import com.progress.berg.entity.AccumulateDeals;
import com.progress.berg.entity.SourceDeals;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.util.Map;

/**
 * @author sds
 */

@Repository
@Transactional
public class AccumulativeDealDaoImpl implements AccumulativeDealDao {

    Logger logger = LoggerFactory.getLogger(AccumulativeDealDaoImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public AccumulativeDealDaoImpl(EntityManager entityManager) {
        em = entityManager;
    }


    @Override
    public void increaseCount(Map<String, Long> dealPerCurrencies, SourceDeals sourceDeals) {
        for (Map.Entry<String, Long> entry : dealPerCurrencies.entrySet()) {
            try {
                AccumulateDeals accumulateDeals = this.getAccumulateDealsByCurrencyCode(entry.getKey());
                accumulateDeals.setDealCount(accumulateDeals.getDealCount() + entry.getValue().intValue());
                this.update(accumulateDeals);
            } catch (NoResultException ex) {
                AccumulateDeals accumulateDeals = new AccumulateDeals();
                accumulateDeals.setCurrencyCode(entry.getKey());
                accumulateDeals.setDealCount(entry.getValue().intValue());
                this.save(accumulateDeals, sourceDeals);

            }
        }

    }

    @Override
    public boolean save(AccumulateDeals accumulateDeals, SourceDeals sourceDeals) {
        try {
            accumulateDeals.setSourceDeals(sourceDeals);
            em.persist(accumulateDeals);
            return true;
        } catch (PersistenceException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                AccumulateDeals acDeals = this.getAccumulateDealsByCurrencyCode(accumulateDeals.getCurrencyCode());
                acDeals.setDealCount(acDeals.getDealCount() + accumulateDeals.getDealCount());
                return this.update(acDeals);
            }
            throw e;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }

        return false;
    }

    @Override
    public boolean update(AccumulateDeals accumulateDeals) {
        try {
            em.merge(accumulateDeals);
            return true;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }
        return false;
    }

    @Override
    public AccumulateDeals getAccumulateDealsByCurrencyCode(String currencyCode) {
        return em.createQuery("Select dpc FROM AccumulateDeals dpc where dpc.currencyCode=:code", AccumulateDeals.class)
                .setParameter("code", currencyCode)
                .getSingleResult();

    }

}
