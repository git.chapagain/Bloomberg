/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.dao.impl;

import com.progress.berg.dao.SourceDealDao;
import com.progress.berg.entity.DealsSummaryDto;
import com.progress.berg.entity.SourceDeals;
import com.progress.berg.entity.ValidDeals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


/**
 * @author sds
 */

@Repository
@Transactional
public class SourceDealDaoImpl implements SourceDealDao {

    Logger logger = LoggerFactory.getLogger(SourceDealDaoImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public SourceDealDaoImpl(EntityManager entityManager) {
        em = entityManager;
    }

    @Override
    public boolean saveFile(SourceDeals source) {
        try {
            em.persist(source);
            return true;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }
        return false;
    }

    @Override
    public boolean updateFile(SourceDeals source) {
        try {
            em.merge(source);
            return true;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }
        return false;
    }

    @Override
    public boolean isFileExist(String fileName) {
        try {
            Object singleResult = em.createQuery("SELECT s from SourceDeals s where s.fileName=:fileName")
                    .setParameter("fileName", fileName)
                    .setMaxResults(1)
                    .getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public List<ValidDeals> getAllDealsByFileName(String fileName, Integer page) {
        return em.createQuery("Select d FROM ValidDeals d where d.sourceDeals.fileName=:fileName")
                .setParameter("fileName", fileName)
                .setMaxResults(20)
                .setFirstResult(20 * (page - 1))
                .getResultList();
    }

    @Override
    public DealsSummaryDto getDealSummaryDto(String fileName) {
        try {
            SourceDeals sourceDeals = (SourceDeals) em.createQuery("select s from SourceDeals s where s.fileName=:fileName").setParameter("fileName", fileName).getSingleResult();
            Long validCount = (Long) em.createQuery("select count(v.id) from ValidDeals v where v.sourceDeals.id=:id").setParameter("id", sourceDeals.getId()).getSingleResult();
            Long invalidCount = (Long) em.createQuery("select count(i.id) from InvalidDeals i where i.sourceDeals.id=:id").setParameter("id", sourceDeals.getId()).getSingleResult();

            DealsSummaryDto dealsSummaryDto = new DealsSummaryDto();
            dealsSummaryDto.setFileName(sourceDeals.getFileName());
            dealsSummaryDto.setTotalImportedDeal(validCount + invalidCount);
            dealsSummaryDto.setValidDeal(validCount);
            dealsSummaryDto.setInvalidDeal(invalidCount);
            dealsSummaryDto.setStartTime(sourceDeals.getStartDate());
            dealsSummaryDto.setEndTime(sourceDeals.getEndDate());
            dealsSummaryDto.setProcessTime((sourceDeals.getEndDate().getTime() - sourceDeals.getStartDate().getTime()) / 1000 % 60);

            return dealsSummaryDto;
        } catch (NoResultException nre) {
            return null;
        }
    }

}
