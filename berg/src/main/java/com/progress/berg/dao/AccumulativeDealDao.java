/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.dao;

import com.progress.berg.entity.AccumulateDeals;
import com.progress.berg.entity.SourceDeals;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 *
 * @author sds
 */

public interface AccumulativeDealDao{

    public void increaseCount(Map<String, Long> dealPerCurrency, SourceDeals sourceDeals);

    public boolean save(AccumulateDeals dealPerCurrency, SourceDeals sourceDeals);

    public boolean update(AccumulateDeals dealPerCurrency);

    public AccumulateDeals getAccumulateDealsByCurrencyCode(String currencyCode);
}
