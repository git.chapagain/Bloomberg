/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.utils;

import java.util.UUID;

/**
 *
 * @author sds
 */
public class SampleUniqueIdGenerator {

    public static final String getRandomUniqueId() {
        return UUID.randomUUID().toString();
    }
}
