/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.utils;

import com.progress.berg.entity.InvalidDeals;
import com.progress.berg.entity.ValidDeals;
import com.progress.berg.exception.InvalidDataException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author sds
 */
public class ValidationUtil {

    public static boolean isValidateAmount(String amount) {
        try {
            Double.parseDouble(amount);
        } catch (Exception p) {
            throw new InvalidDataException("Invalid amount !");
        }
        return true;
    }

    public static boolean isValidateDate(String dateString) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.parse(dateString);
        } catch (ParseException e) {
            throw new InvalidDataException("Invalid data format !");
        }
        return true;
    }

    public static ValidDeals getValidDeal(String data) {
        String[] realData = data.split(",");
        if (realData.length != 5) {
            throw new InvalidDataException("Invalid data lenght !");
        }

        CurrencyCodeGenerator.validateCurrencyCode(realData[1]);
        CurrencyCodeGenerator.validateCurrencyCode(realData[2]);
        isValidateDate(realData[3]);
        isValidateAmount(realData[4]);

        ValidDeals validDeals = new ValidDeals();
        validDeals.setUniqueId(realData[0]);
        validDeals.setFromCurrency(realData[1]);
        validDeals.setToCurrency(realData[2]);
        validDeals.setDealDate(realData[3]);
        validDeals.setAmount(realData[4]);

        return validDeals;

    }

    public static InvalidDeals getInValidDeal(String data) {
        String[] realData = data.split(",");
        InvalidDeals invalidDeals = new InvalidDeals();
        invalidDeals.setUniqueId(realData[0]);
        invalidDeals.setFromCurrency(realData[1]);
        invalidDeals.setToCurrency(realData[2]);
        invalidDeals.setDealDate(realData[3]);
        invalidDeals.setAmount(realData[4]);

        return invalidDeals;

    }

}
