/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author sds
 */
public class DateFormatter {

    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static String getDate() {
        return new SimpleDateFormat(DATE_PATTERN).format(new Date());
    }

}
