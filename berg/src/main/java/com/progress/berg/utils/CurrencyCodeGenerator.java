/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.utils;

import com.progress.berg.exception.InvalidDataException;

import java.util.Arrays;
import java.util.Random;

/**
 * @author sds
 */
public class CurrencyCodeGenerator {

    private static final String[] COUNTRY_CODE = new String[]{"AED", "AUS", "INR", "USD", "CAD", "NPR"};
    private static final String KEY = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final Random RANDOM = new Random();

    public static void validateCurrencyCode(final String countryCode){
        if (!Arrays.stream(COUNTRY_CODE).anyMatch(e -> e.equals(countryCode))) {
            throw new InvalidDataException("Invalid Currency Code: " + countryCode);
        }
    }

    public static String getRandomValidCurrencyCode() {
        return COUNTRY_CODE[new Random().nextInt(COUNTRY_CODE.length)];
    }

    public static String getRandomInvalidCurrencyCode() {
        char[] text = new char[3];
        for (int i = 0; i < 3; i++) {
            text[i] = KEY.charAt(RANDOM.nextInt(KEY.length()));
        }
        return new String(text);
    }
}
