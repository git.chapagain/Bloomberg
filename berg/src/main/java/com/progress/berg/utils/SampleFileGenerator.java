package com.progress.berg.utils;

import com.progress.berg.config.singleton.PropertiesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SampleFileGenerator {

    static final Logger LOGGER = LoggerFactory.getLogger(SampleFileGenerator.class);


    public static String generateSmaple() {
        FileWriter writer = null;
        String result=null;
        try {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYmmdd-HHmmss");
            String date=simpleDateFormat.format(new Date());

            PropertiesManager apm = PropertiesManager.getInstance();
            String csvSample = apm.getProperty("app.filepath.sample-location") + File.separator + "sourcedeal-" + date + ".csv";

            writer = new FileWriter(csvSample);

            //for header
            CSVUtils.writeLine(writer, Arrays.asList("UNIQUE_ID", "FROM_CURRENCY", "TO_CURRENCY", "DEAL_DATE", "AMOUNT"));

            for (int i = 0; i < 100000; i++) {
                List<String> deals = new ArrayList<>();
                if (i % 2 == 0) {
                    deals.add(SampleUniqueIdGenerator.getRandomUniqueId());
                    deals.add(CurrencyCodeGenerator.getRandomValidCurrencyCode());
                    deals.add(CurrencyCodeGenerator.getRandomValidCurrencyCode());
                    deals.add(DateFormatter.getDate());
                    deals.add(String.valueOf(SampleAmountGenerator.getRandomAmount()));
                } else {
                    deals.add(SampleUniqueIdGenerator.getRandomUniqueId());
                    deals.add(CurrencyCodeGenerator.getRandomInvalidCurrencyCode());
                    deals.add(CurrencyCodeGenerator.getRandomInvalidCurrencyCode());
                    deals.add(DateFormatter.getDate());
                    deals.add("-" + String.valueOf(SampleAmountGenerator.getRandomAmount()));
                }
                CSVUtils.writeLine(writer, deals);

                result= "Sample file sourcedeal-"+date+".csv generate successfully.";
            }
            LOGGER.info("Sample file " + "sourcedeal-" + date + ".csv" + " generated successfully!!");
        } catch (IOException ex) {
            ex.printStackTrace();
            LOGGER.error("Error on sample generate");
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage());
            }
        }
        return result;
    }
}
