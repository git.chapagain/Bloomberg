/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progress.berg.utils;

import java.util.Random;

/**
 *
 * @author sds
 */
public class SampleAmountGenerator {

    private static final Random RANDOM = new Random();
    private static final double MIN_AMOUNT = 10;
    private static final double MAX_AMOUNT = 10000;

    public static double getRandomAmount() {
        return Math.round(MIN_AMOUNT + (MAX_AMOUNT - MIN_AMOUNT) * RANDOM.nextDouble() * 100000d) / 100000d;
    }
}
