<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <link href="../../resources/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../resources/css/bloomberg.css" rel="stylesheet" type="text/css"/>
</head>
<body ng-app="demo">
<h1>Bloom-Berg</h1>

<h3>
    <c:if test="${not empty success}">
        <span style="color: green">${success}</span>
    </c:if>
    <c:if test="${not empty error}">
        <span style="color: red">${error}</span>
    </c:if>
</h3>
<div style="background-color: blanchedalmond;width: 80%;">
    <spring:url value="/uploadFile" var="upload" htmlEscape="true"/>
    <form method="POST" action="${upload}" enctype="multipart/form-data">

        <div class="col-lg-3"><input type="file" name="file" accept=".csv"/>choses file</div>
        <div class="col-lg-3"><input type="submit" value="Upload"/></div>
    </form>

    <spring:url value="/sample" var="sample" htmlEscape="true"/>
    <form method="POST" action="${sample}" enctype="multipart/form-data">
        <div class="col-lg-3"><input type="submit" value="Generate Sample"/></div>
    </form>

</div>


<div style="width: 80%;">

    <div>
        <div class="search">
            <spring:url value="/searchsummary" var="search" htmlEscape="true"/>
            <form method="GET" action="${search}">
                <div class="col-lg-3">
                    <input type="text" name="query"/>
                </div>
                <div class="col-lg-3">
                    <input type="submit" value="search"/>
                </div>
            </form>
        </div>
    </div>
    <c:if test="${not empty noresult}"><span style="color: red;">${noresult}</span></c:if>
    <c:if test="${not empty summary}">
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>FileName</th>
                <th>Imported Deals</th>
                <th>Valid Deals</th>
                <th>Invalid Deals</th>
                <th>Process Start</th>
                <th>Process End</th>
                <th>Total Process Time</th>
            </tr>
            </thead>

            <tbody>
                <%--<c:forEach items="${summary}" var="summary">--%>
            <tr>
                <td>${summary.fileName}</td>
                <td>${summary.totalImportedDeal}</td>
                <td>${summary.validDeal}</td>
                <td>${summary.invalidDeal}</td>
                <td>${summary.startTime}</td>
                <td>${summary.endTime}</td>
                <td>${summary.processTime}. Seconds</td>
            </tr>
                <%--</c:forEach>--%>
            </tbody>
        </table>
    </c:if>
</div>
</form>


<script src="../../resources/javascript/jquery-1.12.4.js" type="text/javascript"></script>
<script src="../../resources/javascript/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../../resources/javascript/bloomberg.js" type="text/javascript"></script>
</body>
</html>
