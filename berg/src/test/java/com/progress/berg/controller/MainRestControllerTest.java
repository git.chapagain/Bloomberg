package com.progress.berg.controller;

import com.progress.berg.BergApplication;
import com.progress.berg.config.singleton.PropertiesManager;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

@SpringBootTest(classes = { BergApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class MainRestControllerTest {

    private MockMvc mockMvc;
    private String path;

    @Before
    public void setUp() throws Exception {
        path=PropertiesManager.getInstance().getProperty("app.filepath.sample-location") + "sourcedeal-20174521-084521.csv";
        mockMvc = MockMvcBuilders
                .standaloneSetup(new MainRestController())
                .build();
    }

    @Test
    public void testUploadFile() throws Exception {
        InputStream inputStream =
                new FileInputStream(path);

        MockMultipartFile file = new MockMultipartFile("file","sourcedeal-20174521-084521.csv",null, inputStream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/uploadFile").file(file))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());

        Assert.assertTrue(new File(path).exists());
    }

    @Test
    public void testGenerateSample()throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.request(HttpMethod.POST, "/sample"))

                .andExpect(MockMvcResultMatchers.status().isOk());

        Assert.assertEquals(200, HttpStatus.OK.value());
    }

}