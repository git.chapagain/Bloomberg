package com.progress.berg.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
public class SourceDealDaoTest {
    private final String FILENAME="sourcedeal-20174521-084521.csv";

    @Mock
    SourceDealDao sourceDealDao;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testFileExist() throws Exception {
        Assert.assertTrue(sourceDealDao.isFileExist(FILENAME));
    }

    @Test
    public void testFileNotExist() throws Exception {
        Assert.assertFalse(sourceDealDao.isFileExist(FILENAME));
    }


    @Test
    public void testGetAllDealsByFileNameSuccess() throws Exception {
        Assert.assertTrue(sourceDealDao.getAllDealsByFileName(FILENAME,1).size()>1);
    }

    @Test
    public void testGetAllDealsByFileNameFailure() throws Exception {
        Assert.assertEquals(sourceDealDao.getAllDealsByFileName(FILENAME,1).size(),0);
    }

    @Test
    public void testGetDealSummaryDtoSuccess() throws Exception {
        Assert.assertNotNull(sourceDealDao.getDealSummaryDto(FILENAME));
        Assert.assertSame(sourceDealDao.getDealSummaryDto(FILENAME).getFileName(), FILENAME);
    }

    @Test
    public void testGetDealSummaryDtoFailure() throws Exception {
        Assert.assertNull(sourceDealDao.getDealSummaryDto(FILENAME));
    }

}