package com.progress.berg.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.progress.berg.entity.AccumulateDeals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
public class AccumulativeDealDaoTest {

    @Mock
    AccumulativeDealDao accumulativeDealDao;

    @Test
    public void testGetAccumulativeDealsWhenExist() throws Exception {
        AccumulateDeals accumulateDeals = accumulativeDealDao.getAccumulateDealsByCurrencyCode("AUS");
        assertNotNull(accumulateDeals);
        assertEquals("AUS", accumulateDeals.getCurrencyCode());
    }

    @Test
    public void testGetAccumulativeDealsWhenDoesNotExist() throws Exception {
        AccumulateDeals accumulateDeals = accumulativeDealDao.getAccumulateDealsByCurrencyCode("AUS");
        assertNull(accumulateDeals);
    }

}