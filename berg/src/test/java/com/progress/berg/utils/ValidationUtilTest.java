package com.progress.berg.utils;

import com.progress.berg.exception.InvalidDataException;
import org.junit.Assert;
import org.junit.Test;

public class ValidationUtilTest {

    /**
     * Test of validateAmount method, of class ValidationUtil.
     */

    @Test
    public void testValidAmountWhenInteger() {
        ValidationUtil.isValidateAmount("12");
        Assert.assertTrue(true);
    }

    @Test
    public void testValidAmountWhenDouble() {
        ValidationUtil.isValidateAmount("12");
        Assert.assertTrue(true);
    }

    @Test(expected = InvalidDataException.class)
    public void testInvalidAmountWhenAlphabetContainsThenExceptionIsThrown() {
        ValidationUtil.isValidateAmount("1a");
    }

    @Test(expected = InvalidDataException.class)
    public void testInvalidAmountWhenNegetiveThenExceptionIsThrown() {
        ValidationUtil.isValidateAmount("-12.9");
    }


    /**
     * Test of validateDate method, of class ValidationUtil.
     */

    @Test
    public void testValidDateTime() {
        ValidationUtil.isValidateDate("2017-12-21 08:45:21");
        Assert.assertTrue(true);
    }

    @Test(expected = InvalidDataException.class)
    public void testInvalidDateTime() {
        ValidationUtil.isValidateDate("fdfd 10:11:32");
        ValidationUtil.isValidateDate("2012-12-01");
    }

}