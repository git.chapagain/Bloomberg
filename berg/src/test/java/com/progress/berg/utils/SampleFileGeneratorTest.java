package com.progress.berg.utils;

import com.progress.berg.config.singleton.PropertiesManager;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class SampleFileGeneratorTest{

    private static int expectedValidRow;
    private static int expectedInvalidRow;
    private static boolean expected;


    @BeforeClass
    public static void setUp() throws Exception {
        FileWriter writer = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYmmdd-HHmmss");
            PropertiesManager apm = PropertiesManager.getInstance();
            String csvSample = apm.getProperty("app.filepath.sample-location") + File.separator + "sourcedeal-" + simpleDateFormat.format(new Date()) + ".csv";

            writer = new FileWriter(csvSample);

            //for header
            CSVUtils.writeLine(writer, Arrays.asList("UNIQUE_ID", "FROM_CURRENCY", "TO_CURRENCY", "DEAL_DATE", "AMOUNT"));

            expectedInvalidRow = 1;
            expectedValidRow = 1;
            for (int i = 0; i < 100000; i++) {
                List<String> deals = new ArrayList<>();
                if (i % 2 == 0) {
                    deals.add(SampleUniqueIdGenerator.getRandomUniqueId());
                    deals.add(CurrencyCodeGenerator.getRandomValidCurrencyCode());
                    deals.add(CurrencyCodeGenerator.getRandomValidCurrencyCode());
                    deals.add(DateFormatter.getDate());
                    deals.add(String.valueOf(SampleAmountGenerator.getRandomAmount()));
                    expectedValidRow++;
                } else {
                    deals.add(SampleUniqueIdGenerator.getRandomUniqueId());
                    deals.add(CurrencyCodeGenerator.getRandomInvalidCurrencyCode());
                    deals.add(CurrencyCodeGenerator.getRandomInvalidCurrencyCode());
                    deals.add(DateFormatter.getDate());
                    deals.add("-" + String.valueOf(SampleAmountGenerator.getRandomAmount()));
                    expectedInvalidRow++;
                }
                CSVUtils.writeLine(writer, deals);

                expected = true;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException ex) {
            }
        }

    }

    @Test
    public void testGenerateSmapleGenerateConfirm() {
        Assert.assertEquals(expected, true);
    }


    @Test
    public void testGenerateSmapleConfirmValidDeals() {
        Assert.assertEquals(expectedValidRow > 40000, true);
        Assert.assertFalse(expectedValidRow < 0);
    }

    @Test
    public void testGenerateSmapleConfirmInValidDeals() {
        Assert.assertEquals(expectedInvalidRow > 40000, true);
        Assert.assertFalse(expectedInvalidRow < 0);
    }

}