package com.progress.berg.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SampleUniqueIdGeneratorTest {
    String randomNumber = "";

    @Before
    public void setUp() {
        randomNumber = SampleUniqueIdGenerator.getRandomUniqueId();
    }

    @Test
    public void testRandomUniqueIdWhenNotEmpty() {
        Assert.assertNotNull("e2027e2f-c461-4d30-9d45-e011aa7275a0");
    }

    @Test
    public void testRandomUniqueIdWhenUnique() {
        Assert.assertNotSame(SampleUniqueIdGenerator.getRandomUniqueId(), randomNumber);
    }


}