package com.progress.berg.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class SampleAmountGeneratorTest {
    private static final Random RANDOM = new Random();
    private static final double MIN_AMOUNT = 10;
    private static final double MAX_AMOUNT = 10000;

    private double randomAmount;

    @Before
    public void getRandomAmount(){
        randomAmount= Math.round(MIN_AMOUNT + (MAX_AMOUNT - MIN_AMOUNT) * RANDOM.nextDouble() * 100000d) / 100000d;
    }

    @Test
    public void testValidRandomAmountWhenLessThanTenThousand() throws Exception {
        Assert.assertTrue(randomAmount<=10000);
    }

    @Test
    public void testValidRandomAmountWhenGreterThanTen() throws Exception {
        Assert.assertTrue(randomAmount>10);
    }

    @Test
    public void testInvalidRandomAmountWhenGreterThanTen() throws Exception {
        Assert.assertFalse(randomAmount<10);
    }

    @Test
    public void testInvalidRandomAmountWhenGreterThanTenThousand() throws Exception {
        Assert.assertFalse(randomAmount>10000);
    }

    @Test
    public void testInvalidRandomAmountWhenContainAlphabate() throws Exception {
        Assert.assertEquals(Pattern.compile("^(?:[1-9][0-9]{1,4}(?:\\.\\d{1,2})?|10|10000.00)$").matcher(String.valueOf("123jj.00")).matches(), false);
    }

}