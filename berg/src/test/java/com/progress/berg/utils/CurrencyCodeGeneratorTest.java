package com.progress.berg.utils;

import com.progress.berg.exception.InvalidDataException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

public class CurrencyCodeGeneratorTest {
    private static final String[] COUNTRY_CODE = new String[]{"AED", "AUS", "INR", "USD", "CAD", "NPR"};
    private static final String KEY = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final Random RANDOM = new Random();

    private String validCurrencyCode;
    private String invalidCurrencyCode;

    @Before
    public void init() {
        validCurrencyCode = COUNTRY_CODE[new Random().nextInt(COUNTRY_CODE.length)];
        char[] text = new char[3];
        for (int i = 0; i < 3; i++) {
            text[i] = KEY.charAt(RANDOM.nextInt(KEY.length()));
        }
        invalidCurrencyCode = new String(text);
    }

    @Test
    public void testValidateCurrencyCode() {
        Assert.assertTrue(Arrays.stream(COUNTRY_CODE).anyMatch(e -> e.equals(validCurrencyCode)));
    }

    @Test
    public void testInValidateCurrencyCode() {
        Assert.assertFalse(Arrays.stream(COUNTRY_CODE).anyMatch(e -> e.equals(invalidCurrencyCode)));
    }

    @Test
    public void testValidateCurrencyCodeWhenLengthEqualsThree() {
        Assert.assertTrue(Arrays.stream(COUNTRY_CODE).anyMatch(e -> e.length() == 3));
    }

    @Test
    public void testRandomInValidCurrencyCodeWhenLengthNotEqualsThree() {
        Assert.assertFalse(Arrays.stream(COUNTRY_CODE).anyMatch(e -> e.length() != 3));
    }

    @Test
    public void testValidateCurrencyCodeWhenCurrencyCodeMatched() {
        CurrencyCodeGenerator.validateCurrencyCode("AUS");
    }

    @Test(expected = InvalidDataException.class)
    public void testValidateCurrencyCodeWhenCurrencyNotMatchThenException() {
        CurrencyCodeGenerator.validateCurrencyCode("PQR");
    }

}